`include "decoder.v"
`include "register_file.v"
`include "alu.v"
`include "ram.v"
`include "control.v"
`include "alu_control.v"
`include "immgen.v"
`include "mem_control.v"
`include "sign_extend.v"

module cpu(
           input         clk,
           input         rst,
           // input [31:0]  instruction,
           output [31:0] pc
           );

   // decoder
   reg [31:0]            r_pc, n_pc, pc_4, pc_imm, pc_add;
   wire [31:0]           immediate;
   wire [4:0]            rs1, rs2, rd;
   wire [2:0]            fnct3;
   wire [6:0]            fnct7;
   wire [6:0]            opcode;

   //alu
   wire [31:0]           result;
   wire [31:0]           data1, data2;
   wire                  cmp;

   reg [31:0]            data;
   reg [31:0]            mux_data1, mux_data2;
   reg [31:0]            mux_data_mem;
   
   wire [4:0]            op_alu;
   wire [31:0]           ram_data;

   // RAM memory
   wire                  zero_extend;
   wire [3:0]            size;
   wire [31:0]           extended;
   
   // control
   wire branch_x, jumpx, reg_jumpx;
   wire mem_writex, mem_readx;
   wire mem_to_regx, return_addrx;
   wire [2:0]            alu_opx;
   wire       alu_immx, alu_pcx;
   wire       reg_writex;
   wire [2:0] instruction_type;
   
   
   wire [31:0] instruction;
   reg [12:0]  cntrl;
   
   assign branch_x    = cntrl[2];
   assign jumpx       = cntrl[1];
   assign reg_jumpx   = cntrl[0];
   
   assign mem_writex  = cntrl[4];
   assign mem_readx   = cntrl[3];

   assign mem_to_regx  = cntrl[6];
   assign return_addrx = cntrl[5];
   
   assign alu_opx     = cntrl[9:7];

   assign alu_pcx     = cntrl[11];
   assign alu_immx    = cntrl[10];
   
   assign reg_writex  = cntrl[12];

   always @(posedge clk or posedge rst) begin
      if (rst)
        r_pc <= 0;
      else
        r_pc <= n_pc;
   end

   always @* begin : next_pc_logic
      pc_4   = r_pc + 1;
      pc_imm = r_pc + immediate;

      pc_add = (branch_x & cmp) | jumpx ? pc_imm : pc_4;
      n_pc   = reg_jumpx                ? result : pc_add;
   end

   ram instruction_mem(
                       .clk    (clk),
                       .addr   (r_pc),
                       .i_data (),
                       .en_w   (4'b000),
                       .en_r   (4'b1111),
                       .o_data (instruction)
                       );

   decoder decoder_cpu(
                       .instruction (instruction),
                       .rs1         (rs1),
                       .rs2         (rs2),
                       .rd          (rd),
                       .imm         (),
                       .funct3      (fnct3),
                       .funct7      (fnct7),
                       .opcode      (opcode)
                       );

   control control_cpu(
                       .opcode (opcode),
                       .ctrl   (cntrl),
                       .i_type (instruction_type)
                       );

   alu_control alu_control_cpu(
                               .alu_op    (alu_opx),
                               .fnct7     (fnct7),
                               .fnct3     (fnct3),
                               .alu_cntrl (op_alu)
                               );

   immgen immgen_cpu(
                     .instruction (instruction),
                     .i_type      (instruction_type),
                     .immediate   (immediate)
                     );

   register_file reg_file(
                          .clk (clk),
//                          .rst (rst),
                          .rs1 (rs1),
                          .rs2 (rs2),
                          .rd (rd),
                          .i_data (data),
                          .w_en (reg_writex),
                          .o_data1 (data1),
                          .o_data2 (data2)
                          );

   always @* begin : alu_src_logic_
      mux_data1 = alu_pcx  ? r_pc      : data1;
      mux_data2 = alu_immx ? immediate : data2;
   end
   
   alu alu_cpu(
               .data1  (mux_data1),
               .data2  (mux_data2),
               .op_alu (op_alu),
               .cmp    (cmp),
               .result (result)
               );

   ram ram_cpu(
               .clk (clk),
               .addr (result),
               .i_data (data2),
               .en_w ({mem_writex & size[3], 
                       mem_writex & size[2], 
                       mem_writex & size[1], 
                       mem_writex & size[0]}),
               .en_r ({mem_readx  & size[3], 
                       mem_readx  & size[2], 
                       mem_readx  & size[1], 
                       mem_readx  & size[0]}),
               // .en_r ({mem_readx& 1'b1,mem_readx&1'b1,mem_readx&1'b1,mem_readx&1'b1}),
               // .en_w (0),
               // .en_r (0),
               .o_data (ram_data)
               );

   mem_control mem_control_cpu(
                               .fnct3 (fnct3),
                               .zero_extend (zero_extend),
                               .mem_cntl (size)
                               );

   sign_extend sign_extend_cpu(
                               .i_data (ram_data),
                               .zero_extend (zero_extend),
                               .size (size),
                               .o_extended (extended)
                               );


   always @* begin : reg_file_src_
      mux_data_mem = mem_to_regx  ? extended : result;
      data         = return_addrx ? pc_4     : mux_data_mem;
   end
   
   
   // outputs
   assign pc = r_pc;

endmodule // cpu
