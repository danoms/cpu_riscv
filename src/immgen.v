// `include "cpu_defines.v"
module immgen(
              input [31:0]      instruction,
              input [2:0]       i_type,
              output reg [31:0] immediate
              );

   always @* begin
     immediate = 0;

     case(i_type)
       R_TYPE: begin 
       end

       I_TYPE: begin 
          immediate = { {20{instruction[31]}}, instruction[31:20]};
       end

       S_TYPE: begin
          immediate = { {20{instruction[31]}}, instruction[31:25], instruction[11:7] };
       end

       B_TYPE: begin
          immediate = { {20{instruction[31]}}, instruction[7], 
                        instruction[30:25], instruction[11:8], 1'b0 };
       end

       U_TYPE: begin
          immediate = { instruction[31:12] , 12'b0 };
       end
          
       J_TYPE: begin 
          immediate = { {12{instruction[31]}}, instruction[19:12], 
                        instruction[20], instruction[30:21], 1'b0 };
       end
          
     endcase // case (i_type)
   end
   
endmodule // immgen
