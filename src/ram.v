module ram(
           input             clk,
           input [31:0]      addr,
           input [31:0]      i_data,
           input [3:0]       en_w,
           input [3:0]       en_r,
           output reg [31:0] o_data
           );

   parameter MEM_SIZE = 100;
   /* verilator lint_off UNOPTFLAT */
   reg [31:0]                mem [0:MEM_SIZE];
   reg [31:0]                internal;

   initial begin
      mem[0] = 32'h01700793;
      mem[1] = 32'hfef42623;
      mem[2] = 32'h02d00793;
      mem[3] = 32'hfef42423;

      // mem[4] = 32'h0afc80b7; //lui
      // mem[5] = 32'h084d0797; // auipc
      // mem[6] = 32'h00e78067; // jr

      mem[4] = 32'hfe842583;
      // mem[4] = 32'h00e78c63;
      mem[5] = 32'hfec42503;
      mem[6] = 32'h030000ef;
      mem[7] = 32'hfea42223;
      mem[8] = 32'hfec42703;
      mem[9] = 32'hfe442783;
      mem[10] = 32'h00f707b3;
      mem[11] = 32'hfef42623;
      mem[12] = 32'h00000793;
      mem[13] = 32'h00078513;
      mem[14] = 32'h01c12083;
      mem[15] = 32'h01812403;
      mem[16] = 32'h02010113;
      mem[17] = 32'h00008067;
   end

   always @* begin
      internal = mem[addr];
      if (en_w[0])
          internal[7:0] = i_data[7:0];
      if (en_w[1])
          internal[15:8] = i_data[15:8];
      if (en_w[2])
          internal[23:16] = i_data[23:16];
      if (en_w[3])
          internal[31:24] = i_data[31:24];
   end 
   
   always @(posedge clk) begin
      if (|en_w)
        mem[addr] <= internal;
        // mem[addr] <= i_data;

      // if (|en_r)
   end
   assign o_data = mem[addr];
   /* verilator lint_on UNOPTFLAT */

endmodule // ram

