`timescale 1ns / 1ps
module sign_extend (
                    input [31:0]      i_data,
                    input             zero_extend,
                    input [3:0]       size,
                    output reg [31:0] o_extended
                    );

   /* verilator lint_off CASEINCOMPLETE */
   always @* begin
      o_extended = i_data;
      
      case(zero_extend)
        1'b0:
          case(size)
            4'b0001: o_extended = { {24{i_data[7]}},  i_data[7:0] };
            4'b0011: o_extended = { {16{i_data[15]}}, i_data[15:0] };
          endcase 
        
        1'b1:
          case(size)
            4'b0001: o_extended = { 24'b0, i_data[7:0] };
            4'b0011: o_extended = { 16'b0, i_data[15:0] };
          endcase 
        
      endcase 
   end
   /* verilator lint_on CASEINCOMPLETE */

endmodule // sign_extend
