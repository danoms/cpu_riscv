// `ifndef CPU_DEFINES
 // `define CPU_DEFINES
 // `include "cpu_defines.v"
// `endif
module alu_control(
                   input [2:0]  alu_op,
                   input [6:0]  fnct7,
                   input [2:0]  fnct3,
                   output reg [4:0] alu_cntrl
                   );

   always @* begin
      alu_cntrl = ADD;

      /* verilator lint_off CASEINCOMPLETE */
      case(alu_op)
        ADD_INST: alu_cntrl = ADD; 
        LUI        : alu_cntrl = PASSTHROUGH;

        BRANCH:     
          case(fnct3)
            3'b000: alu_cntrl = BEQ;
            3'b001: alu_cntrl = BNE;
            3'b100: alu_cntrl = BLT;
            3'b101: alu_cntrl = BGE;
            3'b110: alu_cntrl = BLTU;
            3'b111: alu_cntrl = BGEU;
          endcase // case (fnct3)
        
        OP_IMM: begin
           case(fnct3)
             3'b000: alu_cntrl = ADD;
             3'b010: alu_cntrl = SLT;
             3'b011: alu_cntrl = SLTU;
             3'b100: alu_cntrl = XOR;
             3'b110: alu_cntrl = OR;
             3'b111: alu_cntrl = AND;
             3'b001: alu_cntrl = SLL;
             3'b101: 
               if (fnct7[5])
                 alu_cntrl = SRA;
               else
                 alu_cntrl = SRL;
           endcase // case (fnct3)

        end // case: endcase...
        
        OP: 
          case({fnct7[5], fnct3})
            4'b0000: alu_cntrl = ADD;
            4'b1000: alu_cntrl = SUB;
            4'b0001: alu_cntrl = SLL;
            4'b0010: alu_cntrl = SLT;
            4'b0011: alu_cntrl = SLTU;
            4'b0100: alu_cntrl = XOR;
            4'b0101: alu_cntrl = SRL;
            4'b1101: alu_cntrl = SRA;
            4'b0110: alu_cntrl = OR;
            4'b0111: alu_cntrl = AND;
          endcase // case ({fnct7[5], fnct3})

      endcase // case (alu_op)
   end
   /* verilator lint_on CASEINCOMPLETE */
   
endmodule // alu_control
