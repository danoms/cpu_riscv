module mem_control(
                   input [2:0]  fnct3,
                   output reg      zero_extend,
                   output reg [3:0] mem_cntl
                   );

   always @* begin
      mem_cntl    = 0;
      zero_extend = 0;
      
      /* verilator lint_off CASEINCOMPLETE */
      case(fnct3)
        3'b000: begin : load_byte_
           mem_cntl = 4'b0001;
        end

        3'b001: begin : load_halfword_
           mem_cntl = 4'b0011;
        end
        
        3'b010: begin : load_word_
           mem_cntl = 4'b1111;
        end

        3'b100: begin : load_byte_unsigned_
           mem_cntl    = 4'b0001;
           zero_extend = 1;
        end

        3'b101: begin : load_halfword_unsigned_
           mem_cntl    = 4'b0011;
           zero_extend = 1;
        end
        
      endcase // case (fnct3)
   end
   /* verilator lint_on CASEINCOMPLETE */
   
endmodule // mem_control
