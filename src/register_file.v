`timescale 1ns / 1ps
module register_file(
                     input         clk,
                     input         rst,
                     input [4:0]   rs1,
                     input [4:0]   rs2,
                     input [4:0]   rd,
                     input [31:0]  i_data,
                     input         w_en,
                     output [31:0] o_data1,
                     output [31:0] o_data2
                     );

   localparam REGISTER_COUNT = 32;
   localparam REGISTER_WIDTH = 32;

   reg [REGISTER_WIDTH-1:0]        q[0:REGISTER_COUNT-1];
   reg [REGISTER_WIDTH-1:0]        din[0:REGISTER_COUNT-1];
   reg [REGISTER_WIDTH-1:0]        mux1;
   reg [REGISTER_WIDTH-1:0]        mux2;

   initial begin
      q[8] = 32;
      q[14] = 45;
      q[15] = 0;
   end
   

   /* verilator lint_off WIDTH */
   reg [$clog2(REGISTER_COUNT):0]  i;
   always @(posedge clk or posedge rst)
     for (i=0; i<REGISTER_COUNT; i=i+1) begin
        if (rst)
          q[i] <= 0;
        else
           
          if (w_en)
            if (i == 0)
              q[i] <= 0;
            else
              q[i] <= din[i];
     end // for (i=0; i<REGISTER_COUNT; i=i+1)
   

   always @* begin : demux
      for (i=0; i<REGISTER_COUNT; i=i+1) 
        din[i] = q[i];
      /* verilator lint_on WIDTH */
      
      // if (i == rd)
      // din[i] = i_data;
      // end
      case(rd) 
        0: din[0] = i_data;
        1: din[1] = i_data;
        2: din[2] = i_data;
        3: din[3] = i_data;
        4: din[4] = i_data;
        5: din[5] = i_data;
        6: din[6] = i_data;
        7: din[7] = i_data;
        8: din[8] = i_data;
        9: din[9] = i_data;
        10: din[10] = i_data;
        11: din[11] = i_data;
        12: din[12] = i_data;
        13: din[13] = i_data;
        14: din[14] = i_data;
        15: din[15] = i_data;
        16: din[16] = i_data;
        17: din[17] = i_data;
        18: din[18] = i_data;
        19: din[19] = i_data;
        20: din[20] = i_data;
        21: din[21] = i_data;
        22: din[22] = i_data;
        23: din[23] = i_data;
        24: din[24] = i_data;
        25: din[25] = i_data;
        26: din[26] = i_data;
        27: din[27] = i_data;
        28: din[28] = i_data;
        29: din[29] = i_data;
        30: din[30] = i_data;
        31: din[31] = i_data;
      endcase // case (rd)
   end // always @ *
   
   
   always @* begin : mux1_rs1
      // mux1 = q[0];
      // for (i=0; i<REGISTER_COUNT; i=i+1)  begin
      // if (i==rs1)
      // mux1 = q[i];
      // end // UNMATCHED !!
      case(rs1)
        0: mux1 = q[0];
        1: mux1 = q[1];
        2: mux1 = q[2];
        3: mux1 = q[3];
        4: mux1 = q[4];
        5: mux1 = q[5];
        6: mux1 = q[6];
        7: mux1 = q[7];
        8: mux1 = q[8];
        9: mux1 = q[9];
        10: mux1 = q[10];
        11: mux1 = q[11];
        12: mux1 = q[12];
        13: mux1 = q[13];
        14: mux1 = q[14];
        15: mux1 = q[15];
        16: mux1 = q[16];
        17: mux1 = q[17];
        18: mux1 = q[18];
        19: mux1 = q[19];
        20: mux1 = q[20];
        21: mux1 = q[21];
        22: mux1 = q[22];
        23: mux1 = q[23];
        24: mux1 = q[24];
        25: mux1 = q[25];
        26: mux1 = q[26];
        27: mux1 = q[27];
        28: mux1 = q[28];
        29: mux1 = q[29];
        30: mux1 = q[30];
        31: mux1 = q[31];
      endcase 
   end

   always @* begin : mux2_rs2
      // mux1 = q[0];
      // for (i=0; i<REGISTER_COUNT; i=i+1)  begin
      // if (i==rs1)
      // mux1 = q[i];
      // end // UNMATCHED !!
      case(rs2)
        0: mux2 = q[0];
        1: mux2 = q[1];
        2: mux2 = q[2];
        3: mux2 = q[3];
        4: mux2 = q[4];
        5: mux2 = q[5];
        6: mux2 = q[6];
        7: mux2 = q[7];
        8: mux2 = q[8];
        9: mux2 = q[9];
        10: mux2 = q[10];
        11: mux2 = q[11];
        12: mux2 = q[12];
        13: mux2 = q[13];
        14: mux2 = q[14];
        15: mux2 = q[15];
        16: mux2 = q[16];
        17: mux2 = q[17];
        18: mux2 = q[18];
        19: mux2 = q[19];
        20: mux2 = q[20];
        21: mux2 = q[21];
        22: mux2 = q[22];
        23: mux2 = q[23];
        24: mux2 = q[24];
        25: mux2 = q[25];
        26: mux2 = q[26];
        27: mux2 = q[27];
        28: mux2 = q[28];
        29: mux2 = q[29];
        30: mux2 = q[30];
        31: mux2 = q[31];
      endcase 
   end

   //outputs
   assign o_data1 = mux1;
   assign o_data2 = mux2;

endmodule // register_file
