// `ifndef CPU_DEFINES
 // `define CPU_DEFINES
 // `include "cpu_defines.v"
// `endif
module alu(
           input signed [31:0]      data1,
           input signed [31:0]      data2,
           input [4:0]              op_alu,
           output reg               cmp,
           output reg signed [31:0] result
           );

   reg [31:0]                       unsigned_data1, unsigned_data2;
   assign unsigned_data1 = data1;
   assign unsigned_data2 = data2;

   always @* begin
      cmp    = 0;
      result = 0;

      case(op_alu)
        ADD : result = data1 + data2;
        SUB : result = data1 - data2;
        SLL : result = data1 << data2[4:0];
        SLT : result = {31'b0, data1 < data2};
        SLTU: result = {31'b0, unsigned_data1 < unsigned_data2};
        XOR : result = data1 ^ data2;
        SRL : result = data1 >> data2[4:0];
        // SRA : result = data1 >> data2;
        OR  : result = data1 | data2;
        AND : result = data1 & data2;

        BEQ  : cmp = data1 == data2;
        BNE  : cmp = data1 != data2;
        BLT  : cmp = data1 < data2;
        BGE  : cmp = data1 >= data2;
        BLTU : cmp = unsigned_data1 < unsigned_data2;
        BGEU : cmp = unsigned_data1 >= unsigned_data2;

        PASSTHROUGH : result = data2; // TODO: madeup for LUI, fix needed

        default: result = 0;
      endcase

   end

endmodule // alu
