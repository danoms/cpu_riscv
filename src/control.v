`include "cpu_defines.v"
module control (
                input [6:0]      opcode,
                output [12:0]    ctrl,
                output reg [2:0] i_type
                );
   reg [1:0]                     mem_ctrl  = 0;  // write/read
   reg [1:0]                     reg_src   = 0;   // memory/pc+4
   reg [2:0]                     alu_op    = 0;    
   reg [1:0]                     alu_src   = 0;   // pc/imm
   reg                           reg_write = 0; 
   reg [2:0]                     pc_src    = 0;    // branch/jump/reg_jump
   
   assign ctrl[2:0]   = pc_src;
   assign ctrl[4:3]   = mem_ctrl;
   assign ctrl[6:5]   = reg_src;
   assign ctrl[9:7]   = alu_op;
   assign ctrl[11:10] = alu_src;
   assign ctrl[12]    = reg_write;
   
   always @* begin
      alu_src    = RS1_RS2; // d1 = rs1|pc, d2 = rs2|imm
      reg_src    = ALU;     // x[rd] = alu_rez|mem_data,->pc+4
      pc_src     = NEXT;    // reg_jump/jump/branch
      reg_write  = 0;       // write to reg file
      mem_ctrl   = 2'b00;   // write/read
      
      alu_op     = OP;      // look at fnct3 & fnct7
      i_type     = R_TYPE;  // reg instructions

      /* verilator lint_off CASEINCOMPLETE */
      case(opcode[4:2])
        3'b000: 
          case(opcode[6:5])
            2'b00: begin : LOAD_ 
               alu_src    = RS1_IMM; // immediate
               reg_src    = MEMORY; // memory
               reg_write  = 1;
               mem_ctrl   = READ; // read

               alu_op = ADD_INST;
               i_type = I_TYPE;
            end

            2'b01: begin : STORE_
               alu_src    = RS1_IMM; // 
               mem_ctrl   = WRITE;

               alu_op = ADD_INST;
               i_type = S_TYPE;
            end
            // 2'b10: MADD;
            2'b11: begin : BRANCH_
               pc_src = BRANCHn;

               alu_op = BRANCH;
               i_type = B_TYPE;
            end
            
          endcase // case (opcode[6:5])
        3'b001:
          case(opcode[6:5])
            // 2'b00: LOAD-FP;
            // 2'b01: STORE-FP;
            // 2'b10: MSUB;
            2'b11: begin : JALR_
               alu_src   = RS1_IMM;
               pc_src    = REG_JUMP;
               reg_write = 1;
               reg_src   = RETURN_ADDRESS;

               alu_op = ADD_INST;
               i_type = I_TYPE;
            end
            
          endcase // case (opcode[6:5])
        // 3'b010: 
        // case(opcode[6:5])
        // 2'b00: custom0;
        // 2'b01: custom1;
        // 2'b10: NMSUB;
        // 2'b11: reserved;
        // endcase // case (opcode[6:5])

        3'b011:
          case(opcode[6:5])
            // 2'b00: MISC-MEM;
            // 2'b01: AMO;
            // 2'b10: NMADD;
            2'b11: begin : JAL_
               reg_src   = RETURN_ADDRESS;
               reg_write = 1;
               pc_src    = JUMP;

               alu_op = ADD_INST;
               i_type = J_TYPE;
            end
          endcase // case (opcode[6:5])

        3'b100: 
          case(opcode[6:5])

            2'b00: begin : OP_IMM_
               alu_src    = RS1_IMM;
               reg_write  = 1;

               alu_op = OP_IMM;
               i_type = I_TYPE;
            end

            2'b01: begin : OP_
               reg_write = 1;

               alu_op = OP;
               i_type = R_TYPE;
            end
            
            // 2'b10: OP-FP;
            // 2'b11: SYSTEM;
          endcase // case (opcode[6:5])

        3'b101:
          case(opcode[6:5])
            2'b00: begin : AUIPC_
               alu_src    = PC_IMM;
               reg_write  = 1;

               alu_op = ADD_INST; // rd = pc + imm
               i_type = U_TYPE;
            end
            
            2'b01: begin : LUI_
               alu_src   = RS1_IMM;
               reg_write = 1;

               alu_op = LUI; // TODO: passstrhough, madeup, fix needed
               i_type = U_TYPE;
            end
            
            // 2'b10: reserved;
            // 2'b11: reserved;
          endcase // case (opcode[6:5])
        // 3'b110:
        // case(opcode[6:5])
        // 2'b00: OP-IMM-32;
        // 2'b01: OP-32;
        // 2'b10: custom2_rv128;
        // 2'b11: custom3_rv128;
        // endcase // case (opcode[6:5])

      endcase // case (opcode[4:2])
   end
   /* verilator lint_on CASEINCOMPLETE */


endmodule // control
