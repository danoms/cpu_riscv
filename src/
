`include "cpu_defines.v"
module control (
                input [6:0]      opcode,
                output [8:0]     ctrl,
                output reg [2:0] i_type
                );
   reg                           mem_read;
   reg                           mem_write;
   reg                           mem_to_reg;
   reg [2:0]                     alu_op;
   reg                           alu_src;
   reg                           reg_write;
   reg                           branch_;
   
   assign ctrl[0]   = branch_;
   assign ctrl[1]   = mem_read;
   assign ctrl[2]   = mem_to_reg;
   assign ctrl[5:3] = alu_op;
   assign ctrl[6]   = mem_write;
   assign ctrl[7]   = alu_src;
   assign ctrl[8]   = reg_write;
   
   always @* begin
      alu_src    = 0; // rs2 or imm
      mem_to_reg = 0; // alu rez or mem_data
      reg_write  = 0; // write to reg file
      mem_read   = 0;
      mem_write  = 0;
      branch_    = 0;
      alu_op     = OP;
      i_type     = R_TYPE;

      case(opcode[4:2])
        3'b000: 
          case(opcode[6:5])
            2'b00: begin : LOAD_ 
               alu_src    = 1;
               mem_to_reg = 1;
               reg_write  = 1;
               mem_read   = 1;
               mem_write  = 0;
               branch_    = 0;
               alu_op     = LOAD_STORE;
               i_type     = I_TYPE;
            end

            2'b01: begin : STORE_
               alu_src    = 1;
               mem_to_reg = 0;
               reg_write  = 0;
               mem_read   = 0;
               mem_write  = 1;
               branch_    = 0;
               alu_op     = LOAD_STORE;
               i_type     = S_TYPE;
            end
            // 2'b10: MADD;
            2'b11: begin : BRANCH_
               alu_src    = 0;
               mem_to_reg = 0;
               reg_write  = 0;
               mem_read   = 0;
               mem_write  = 0;
               branch_    = 1;
               alu_op     = BRANCH;
               i_type     = B_TYPE;
            end
            
          endcase // case (opcode[6:5])
        // 3'b001:
        // case(opcode[6:5])
        // 2'b00: LOAD-FP;
        // 2'b01: STORE-FP;
        // 2'b10: MSUB;
        // 2'b11: JALR;
        // endcase // case (opcode[6:5])
        // 3'b010: 
        // case(opcode[6:5])
        // 2'b00: custom0;
        // 2'b01: custom1;
        // 2'b10: NMSUB;
        // 2'b11: reserved;
        // endcase // case (opcode[6:5])
        // 3'b011:
        // case(opcode[6:5])
        // 2'b00: MISC-MEM;
        // 2'b01: AMO;
        // 2'b10: NMADD;
        // 2'b11: JAL;
        // endcase // case (opcode[6:5])
        3'b100: 
          case(opcode[6:5])

            2'b00: begin : OP_IMM_
               alu_src    = 0;
               mem_to_reg = 0;
               reg_write  = 1;
               mem_read   = 0;
               mem_write  = 0;
               branch_    = 0;
               alu_op     = OP_IMM;
               i_type     = I_TYPE;
            end

            2'b01: begin : OP_
               alu_src    = 0;
               mem_to_reg = 0;
               reg_write  = 1;
               mem_read   = 0;
               mem_write  = 0;
               branch_    = 0;
               alu_op     = OP;
               i_type     = R_TYPE;
            end
            
            // 2'b10: OP-FP;
            // 2'b11: SYSTEM;
          endcase // case (opcode[6:5])

        3'b101:
          case(opcode[6:5])
            2'b00: begin : AUIPC_
               alu_src    = 0;
               mem_to_reg = 0;
               reg_write  = 1;
               mem_read   = 0;
               mem_write  = 0;
               branch_    = 0;
               alu_op     = LOAD_STORE; // nop, load_store for now
               i_type     = U_TYPE;
            end
            
            2'b01: begin : LUI_
               alu_src    = 0;
               mem_to_reg = 0;
               reg_write  = 1;
               mem_read   = 0;
               mem_write  = 0;
               branch_    = 0;
               alu_op     = LOAD_STORE; // nop, load_store for now
               i_type     = U_TYPE;
            end
            
            // 2'b10: reserved;
            // 2'b11: reserved;
          endcase // case (opcode[6:5])
        // 3'b110:
        // case(opcode[6:5])
        // 2'b00: OP-IMM-32;
        // 2'b01: OP-32;
        // 2'b10: custom2_rv128;
        // 2'b11: custom3_rv128;
        // endcase // case (opcode[6:5])

      endcase // case (opcode[4:2])
   end

endmodule // control
