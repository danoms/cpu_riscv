localparam // alu_op
  ADD_INST   = 3'b000,
  BRANCH     = 3'b001,
  LUI        = 3'b010,
  // UJ         = 3'b010,
  OP_IMM     = 3'b100,
  OP         = 3'b011;

localparam // alu_cntrl 
  PASSTHROUGH = 5'h1A, // TODO: madeup for LUI, fix needed
  ADD  = 5'h0,
  SUB  = 5'h1,
  SLL  = 5'h2,
  SLT  = 5'h3,
  SLTU = 5'h4,
  XOR  = 5'h5,
  SRL  = 5'h6,
  SRA  = 5'h7,
  OR   = 5'h8,
  AND  = 5'h9;

localparam // branch
  BEQ  = 5'ha,
  BNE  = 5'hb,
  BLT  = 5'hc,
  BGE  = 5'hd,
  BLTU = 5'he,
  BGEU = 5'hf;

localparam // instruction types
  R_TYPE = 0,
  I_TYPE = 1,
  S_TYPE = 2,
  B_TYPE = 3,
  U_TYPE = 4,
  J_TYPE = 5;

localparam // control bits
  RS1_IMM = 2'b01, // alu_src
  RS1_RS2 = 2'b00,
  PC_IMM  = 2'b11,

  MEMORY         = 2'b10, // reg_src
  ALU            = 2'b00,
  RETURN_ADDRESS = 2'b01,

  WRITE = 2'b10, // mem_ctrl
  READ  = 2'b01,

  NEXT     = 3'b000, // pc_src
  BRANCHn  = 3'b100,
  JUMP     = 3'b010,
  REG_JUMP = 3'b001;

  
