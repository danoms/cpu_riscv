module decoder(
               input [31:0]  instruction,
               output [4:0]  rs1,
               output [4:0]  rs2,
               output [4:0]  rd,
               output [31:0] imm,
               output [2:0]  funct3,
               output [6:0]  funct7,
               output [6:0]  opcode
               );

   // 31           25 24      20 19     15 14     12 11          7 6       0 
   // |              |          |         |         |             |        |        |
   // |--------------+----------+---------+---------+-------------+--------+--------|
   // | funct7       | rs2      | rs1     | funct3  | rd          | opcode | R-type |
   // | imm[11:4:    | :3:0]    | rs1     | funct3  | rd          | opcode | I-Type |
   // | imm[11:5]    | rs2      | rs1     | funct3  | imm[4:0]    | opcode | S-type |
   // | imm[12,10:5] | rs2      | rs1     | funct3  | imm[4:1,11] | opcode | B-type |
   // |--------------+----------+---------+---------+-------------+--------+--------|
   // | imm[31:25:   | :24:20:  | :19:15: | :14:12] | rd          | opcode | U-type |
   // | imm[20,10:5: | :4:1,11: | :19:15: | :14:12] | rd          | opcode | J-type |
   // |--------------+----------+---------+---------+-------------+--------+--------|
   
   assign opcode = instruction[6:0];

   assign rs2 = instruction[24:20];
   assign rs1 = instruction[19:15];
   assign rd  = instruction[11:7];

   assign funct3 = instruction[14:12];
   assign funct7 = instruction[31:25];

   assign imm = instruction;


endmodule // decoder
