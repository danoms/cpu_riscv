`include "../../src/register_file.v"
module register_file_tb;
   reg clk;
   reg [4:0] rs1;
   reg [4:0] rs2;
   reg [4:0] rd;
   reg [31:0] i_data;
   reg        w_en;
   wire [31:0] o_data1;
   wire [31:0] o_data2;

   register_file DUT(.*);

   initial begin
      $dumpfile("register_file_tb.vcd");
      $dumpvars;

      #0
        clk = 0;
      i_data = 32'hdeadbeef;

      #100
        rs1 = 10;
      rs2 = 5;
      rd = 2;

      #5 w_en = 1;
      #30 w_en = 0;

      #20
        rs1 = 2;
      rd = 4;
      rs1 = 4;
      w_en = 1;
      #40 w_en = 0;

      #1000 $finish;
   end // initial begin

   always
     #10 clk = !clk;
   

endmodule // register_file_tb
