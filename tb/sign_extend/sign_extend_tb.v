`include "../../src/sign_extend.v"
module sign_extend_tb;
   reg [11:0] imm_short;
   wire [31:0] imm;

   initial begin
      $dumpfile("sign_extend_tb.vcd");
      $dumpvars;

      #20 imm_short = 234;
      #20 imm_short = 11;
      #20 imm_short = -1;
      #20 imm_short = -255;
      
      # 100 $finish;
      
   end // initial begin

   sign_extend DUT(.*);

endmodule // sign_extend_tb
