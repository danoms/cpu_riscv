`include "../../src/cpu.v"
module cpu_tb;
   reg clk, rst;
   reg [31:0] pc;

   cpu DUT(.*);

   initial begin
      $dumpfile("cpu_tb.vcd");
      $dumpvars;

      #0 clk = 0;
      rst = 1;
      #40 rst = 0;
      
      #1000 $finish;
      
   end
   

   always
     #10 clk = !clk;
   
endmodule // cpu_tb
