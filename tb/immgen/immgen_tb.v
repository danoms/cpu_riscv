`include "../../src/immgen.v"
module immgen_tb;
   reg [31:0] instruction;
   reg [2:0]       i_type;
   reg [31:0]      immediate;
   reg             clk;
   
   immgen DUT(.*);

   initial begin
      $dumpfile("immgen_tb.vcd");
      $dumpvars;

      #0 clk = 0;

      #60 i_type = 1; // I-type
      instruction = 32'h02010413;

      #60
        instruction = 32'h82010413; 
      #60
        instruction = 32'hfe010113; 


      #60
        i_type = 3; // B-type
      instruction = 32'h00030663;

      #100 $finish;
      
   end
   
   always
     #10 clk = !clk;
   

endmodule // immgen_tb
