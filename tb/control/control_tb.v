`include "../../src/control.v"
module control_tb;

   reg [6:0] opcode;
   reg [2:0] i_type;
   
   wire [7:0] ctrl;
   reg        clk;
   

   control DUT(.*);

   initial begin
      $dumpfile("control_tb.vcd");
      $dumpvars;

      #0 clk = 0;
      #60 opcode = 32'h00f707b3; // R-type
      #20 opcode = 32'hfec42703; // I-type
      #20 opcode = 32'hfef42623; // S-type
      #20 opcode = 32'h00030663; // B-type
      
      #100 $finish;
   end

   always
     #10 clk = !clk;

endmodule // control_tb
