`include "../../src/decoder.v"
module decoder_tb;
   reg [31:0] instruction;
   wire [4:0] rs1;
   wire [4:0] rs2;
   wire [4:0] rd;
   wire [31:0] imm;
   wire [2:0]  funct3;
   wire [6:0]  funct7;
   
   integer f_input;
   
   integer cnt_input;
   
   reg [31:0] val_output;
   
   initial begin
      $dumpfile("decoder_tb.vcd");
      $dumpvars;
      
      f_input = $fopen("input.txt", "r");
      if (f_input == 0) begin
         $display("input data handel NULL");
         $finish;
      end
   end
   
   always #10 begin 
      if (!$feof(f_input)) 
        begin
           cnt_input = $fscanf(f_input, "%h\n", instruction);
           $display("inputs = %b", instruction);
        end 
      else
        begin
           $finish;
           $fclose(f_input);
        end
   end

   decoder DUT(.*);

endmodule // decoder_tb
