`include "../../src/ram.v"
module ram_tb;
   reg clk;
   reg [31:0] addr;
   reg [31:0] i_data;
   reg [3:0]  en_w;
   reg [3:0]  en_r;
   wire [31:0] o_data;

   ram ram_inst(.*);

   initial begin
      $dumpfile("ram_tb.vcd");
      $dumpvars;

      #0
        clk = 0;
      addr = 0;
      i_data = 0;
      en_w = 0;
      en_r = 0;

      #60
        addr = 4;
      i_data = 32'habbcbeef;
      en_w = 4'b0001;
      #40
        addr = 5;
      i_data = 32'h12345678;
      en_w = 4'b0011;

      #40
        addr = 6;
      i_data = 32'h87654321;
      en_w = 4'b1111;

      #30
        en_w = 0;
      
        addr = 4;
      #40 en_r = 1;
      #10 en_r = 0;
      
      #20
        addr = 5;
      en_r = 1;

      #40 addr = 6;
      #40 addr = 7;
      #40 addr = 8;
      

      #100
        $finish;

   end // initial begin

   always
     #10 clk = !clk;
      
   
endmodule // ram_tb
